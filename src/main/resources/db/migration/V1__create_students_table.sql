CREATE TABLE students
(
    student_id UUID PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    age INT NOT NULL,
    grade INT NOT NULL
);

CREATE TABLE courses
(
    course_id UUID PRIMARY KEY,
    title VARCHAR(64) NOT NULL,
    description VARCHAR(1000) NOT NULL,
    required_grade INT NOT NULL
);

CREATE TABLE enrollment
(
    student_id UUID,
    course_id UUID,
    PRIMARY KEY (student_id, course_id),
    FOREIGN KEY(student_id) REFERENCES students (student_id) ON DELETE CASCADE ,
    FOREIGN KEY(course_id) REFERENCES courses (course_id) ON DELETE CASCADE
);