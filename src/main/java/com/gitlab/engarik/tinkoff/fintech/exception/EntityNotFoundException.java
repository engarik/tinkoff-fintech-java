package com.gitlab.engarik.tinkoff.fintech.exception;

public class EntityNotFoundException extends RuntimeException {

    private static final String TEMPLATE = "%s not found";

    public <T> EntityNotFoundException(Class<T> clazz) {
        super(String.format(TEMPLATE, clazz.getSimpleName()));
    }

}
