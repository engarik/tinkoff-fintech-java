package com.gitlab.engarik.tinkoff.fintech.kafka;

public enum Status {
    CREATED,
    UPDATED,
    DELETED
}
