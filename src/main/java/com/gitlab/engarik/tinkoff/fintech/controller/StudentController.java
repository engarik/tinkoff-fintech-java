package com.gitlab.engarik.tinkoff.fintech.controller;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.StudentRequest;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;


import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tinkoff/fintech/java/api/v1/students")
@Validated
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Student save(@RequestBody @Valid StudentRequest studentRequest) {
        return studentService.save(studentRequest);
    }

    @GetMapping(path = "/{studentId}")
    public Student findById(@PathVariable UUID studentId) {
        return studentService.findById(studentId);
    }

    @GetMapping
    public List<Student> findAll() {
        return studentService.findAll();
    }

    @PutMapping("/{studentId}")
    public void update(@PathVariable UUID studentId, @RequestBody @Valid StudentRequest studentRequest) {
        studentService.update(studentId, studentRequest);
    }

    @DeleteMapping(path = "/{studentId}")
    public void delete(@PathVariable UUID studentId) {
        studentService.delete(studentId);
    }

}
