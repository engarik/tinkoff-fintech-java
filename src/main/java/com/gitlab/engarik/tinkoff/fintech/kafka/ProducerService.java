package com.gitlab.engarik.tinkoff.fintech.kafka;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProducerService {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Value("${custom.kafka.topic}")
    String topic;

    public void produce(String key, String message) {
        kafkaTemplate.send(topic, key, message);
    }
}
