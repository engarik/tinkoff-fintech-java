package com.gitlab.engarik.tinkoff.fintech.controller.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentRequest {

    @NotEmpty
    String name;

    @NotNull
    @Range(min = 18, max = 100, message = "Age should be in range [18;100]")
    Integer age;

    @NotNull
    @Range(min = 0, max = 5, message = "Grade should be in range [0;5]")
    Integer grade;

}
