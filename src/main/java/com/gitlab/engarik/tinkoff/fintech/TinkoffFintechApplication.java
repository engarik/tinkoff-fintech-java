package com.gitlab.engarik.tinkoff.fintech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;


@SpringBootApplication
@ConfigurationPropertiesScan("com.gitlab.engarik.tinkoff.fintech.security")
public class TinkoffFintechApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinkoffFintechApplication.class, args);
    }

}
