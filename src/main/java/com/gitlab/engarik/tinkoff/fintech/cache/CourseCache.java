package com.gitlab.engarik.tinkoff.fintech.cache;

import com.gitlab.engarik.tinkoff.fintech.model.Course;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CourseCache {

    private final Map<UUID, Course> cacheMap;

    public CourseCache() {
        cacheMap = new ConcurrentHashMap<>();
    }

    public void put(Course course) {
        cacheMap.putIfAbsent(course.getId(), course);
    }

    public void update(Course course) {
        cacheMap.replace(course.getId(), course);
    }

    public void remove(UUID courseId) {
        cacheMap.remove(courseId);
    }

    public void clear() {
        cacheMap.clear();
    }

    public Optional<Course> get(UUID courseId) {
        return Optional.ofNullable(cacheMap.get(courseId));
    }

    @Override
    public String toString() {
        return cacheMap.toString();
    }

}
