package com.gitlab.engarik.tinkoff.fintech.repository;

import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface EnrollmentRepository {

    void enroll(UUID studentId, UUID courseId);

    void expel(UUID studentId, UUID courseId);

    List<Course> getCourseList(UUID studentId);

    List<Student> getStudentList(UUID courseId);

    void deleteAll();

}
