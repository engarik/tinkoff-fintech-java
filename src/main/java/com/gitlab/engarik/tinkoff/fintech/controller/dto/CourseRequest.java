package com.gitlab.engarik.tinkoff.fintech.controller.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CourseRequest {

    @NotBlank
    String title;

    @NotBlank
    String description;

    @Range(min = 0, max = 5, message = "RequiredGrade should be in range [0;5]")
    Integer requiredGrade;
}
