package com.gitlab.engarik.tinkoff.fintech.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Enrollment {

    private final UUID studentId;
    private final UUID courseId;

}
