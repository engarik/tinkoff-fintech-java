package com.gitlab.engarik.tinkoff.fintech.validation;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.EnrollmentRequest;
import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.CourseRepository;
import com.gitlab.engarik.tinkoff.fintech.repository.StudentRepository;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class GradeValidator implements ConstraintValidator<ValidGrade, EnrollmentRequest> {

    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;

    @Override
    public boolean isValid(EnrollmentRequest enrollmentRequest, ConstraintValidatorContext constraintValidatorContext) {
        Student student = studentRepository.findById(enrollmentRequest.getStudentId());
        Course course = courseRepository.findById(enrollmentRequest.getCourseId());

        return student.getGrade() >= course.getRequiredGrade();
    }

}
