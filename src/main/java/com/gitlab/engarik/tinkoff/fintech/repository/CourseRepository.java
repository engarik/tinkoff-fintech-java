package com.gitlab.engarik.tinkoff.fintech.repository;

import com.gitlab.engarik.tinkoff.fintech.model.Course;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface CourseRepository {

    void save(Course course);

    Course findById(UUID id);

    List<Course> findAll();

    int update(Course course);

    int delete(UUID id);

    void deleteAll();

}
