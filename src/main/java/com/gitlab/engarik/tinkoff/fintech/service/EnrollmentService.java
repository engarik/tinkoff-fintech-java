package com.gitlab.engarik.tinkoff.fintech.service;

import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.EnrollmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EnrollmentService {

    private final EnrollmentRepository repository;

    public void enroll(UUID studentId, UUID courseId) {
        repository.enroll(studentId, courseId);
    }

    public void expel(UUID studentId, UUID courseId) {
        repository.expel(studentId, courseId);
    }

    public List<Course> getCourseList(UUID studentId) {
        return repository.getCourseList(studentId);
    }

    public List<Student> getStudentList(UUID courseId) {
        return repository.getStudentList(courseId);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
