package com.gitlab.engarik.tinkoff.fintech.service;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.StudentRequest;
import com.gitlab.engarik.tinkoff.fintech.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.kafka.ProducerService;
import com.gitlab.engarik.tinkoff.fintech.kafka.Status;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository repository;
    private final ProducerService producerService;

    public Student save(StudentRequest studentRequest) {
        Student student = Student.builder()
                .name(studentRequest.getName())
                .age(studentRequest.getAge())
                .grade(studentRequest.getGrade())
                .build();

        repository.save(student);

        producerService.produce(Status.CREATED.name(), student.toString());

        return student;
    }

    public Student findById(UUID id) {
        Student found = repository.findById(id);

        if (isNull(found)) {
            throw new EntityNotFoundException(Student.class);
        }

        return found;
    }

    public List<Student> findAll() {
        return repository.findAll();
    }

    public void update(UUID studentId, StudentRequest studentRequest) {
        Student student = Student.builder()
                .id(studentId)
                .name(studentRequest.getName())
                .age(studentRequest.getAge())
                .grade(studentRequest.getGrade())
                .build();

        if (repository.update(student) <= 0) {
            throw new EntityNotFoundException(Student.class);
        }

        producerService.produce(Status.UPDATED.name(), student.toString());

    }

    public void delete(UUID id) {
        if (repository.delete(id) <= 0) {
            throw new EntityNotFoundException(Student.class);
        }

        producerService.produce(Status.DELETED.name(), id.toString());

    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
