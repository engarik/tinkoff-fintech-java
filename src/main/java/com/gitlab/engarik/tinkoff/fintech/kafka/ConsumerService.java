package com.gitlab.engarik.tinkoff.fintech.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

    @KafkaListener(topics = "${custom.kafka.topic}")
    public void consume(@Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key, String message) {
        System.out.println(key + " : " + message);
    }
}
