package com.gitlab.engarik.tinkoff.fintech.repository;

import com.gitlab.engarik.tinkoff.fintech.model.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface StudentRepository {

    void save(Student student);

    Student findById(UUID id);

    List<Student> findAll();

    int update(Student student);

    int delete(UUID id);

    void deleteAll();

}
