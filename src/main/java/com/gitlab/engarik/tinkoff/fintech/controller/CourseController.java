package com.gitlab.engarik.tinkoff.fintech.controller;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.CourseRequest;
import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;


import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/tinkoff/fintech/java/api/v1/courses")
@Validated
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Course save(@RequestBody @Valid CourseRequest courseRequest) {
        return courseService.save(courseRequest);
    }

    @GetMapping(path = "/{id}")
    public Course findById(@PathVariable UUID id) {
        return courseService.findById(id);
    }

    @GetMapping
    public List<Course> findAll() {
        return courseService.findAll();
    }

    @PutMapping("/{id}")
    public void update(@PathVariable UUID id, @RequestBody @Valid CourseRequest courseRequest) {
        courseService.update(id, courseRequest);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        courseService.delete(id);
    }

}
