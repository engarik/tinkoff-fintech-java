package com.gitlab.engarik.tinkoff.fintech.service;

import com.gitlab.engarik.tinkoff.fintech.cache.CourseCache;
import com.gitlab.engarik.tinkoff.fintech.controller.dto.CourseRequest;
import com.gitlab.engarik.tinkoff.fintech.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.repository.CourseRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository repository;

    @Getter
    private final CourseCache courseCache;

    public Course save(CourseRequest courseRequest) {
        Course course = Course.builder()
                .title(courseRequest.getTitle())
                .description(courseRequest.getDescription())
                .requiredGrade(courseRequest.getRequiredGrade())
                .build();

        repository.save(course);
        courseCache.put(course);

        return course;
    }

    public Course findById(UUID id) {
        Course foundCourse = courseCache.get(id).orElse(repository.findById(id));

        if (isNull(foundCourse)) {
            throw new EntityNotFoundException(Course.class);
        }

        courseCache.put(foundCourse);

        return foundCourse;
    }

    public List<Course> findAll() {
        return repository.findAll();
    }

    public void update(UUID id, CourseRequest courseRequest) {
        Course course = Course.builder()
                .id(id)
                .title(courseRequest.getTitle())
                .description(courseRequest.getDescription())
                .requiredGrade(courseRequest.getRequiredGrade())
                .build();

        if (repository.update(course) <= 0) {
            throw new EntityNotFoundException(Course.class);
        } else {
            courseCache.update(course);
        }
    }

    public void delete(UUID id) {
        if (repository.delete(id) <= 0) {
            throw new EntityNotFoundException(Course.class);
        } else {
            courseCache.remove(id);
        }
    }

    public void deleteAll() {
        repository.deleteAll();
        courseCache.clear();
    }

}
