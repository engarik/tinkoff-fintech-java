package com.gitlab.engarik.tinkoff.fintech.controller;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.EnrollmentRequest;
import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.service.EnrollmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tinkoff/fintech/java/api/v1/enrollment")
@Validated
@RequiredArgsConstructor
public class EnrollmentController {

    private final EnrollmentService enrollmentService;

    @PostMapping
    public void enroll(@RequestBody @Valid EnrollmentRequest enrollmentRequest) {
        enrollmentService.enroll(enrollmentRequest.getStudentId(), enrollmentRequest.getCourseId());
    }

    @DeleteMapping
    public void expel(@RequestParam UUID studentId, @RequestParam UUID courseId) {
        enrollmentService.expel(studentId, courseId);
    }

    @GetMapping(path = "/students/{studentId}")
    public List<Course> getCourseList(@PathVariable UUID studentId) {
        return enrollmentService.getCourseList(studentId);
    }

    @GetMapping(path = "/courses/{courseId}")
    public List<Student> getStudentList(@PathVariable UUID courseId) {
        return enrollmentService.getStudentList(courseId);
    }

}
