package com.gitlab.engarik.tinkoff.fintech.controller.dto;

import com.gitlab.engarik.tinkoff.fintech.validation.ValidGrade;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ValidGrade
public class EnrollmentRequest {

    @NotNull
    UUID studentId;

    @NotNull
    UUID courseId;
}
