package com.gitlab.engarik.tinkoff.fintech.controller;

import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
class StudentControllerTest {

    @Autowired
    StudentRepository repository;

    @Autowired
    private MockMvc mvc;

    ObjectMapper jackson = new ObjectMapper();

    private final String URI = "/tinkoff/fintech/java/api/v1/students";

    @AfterEach
    public void clear() {
        repository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        Student student = createDefaultStudent();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(student))
                )
                .andExpect(status().isCreated());

    }

    @Test
    void saveValidationFail() throws Exception {
        Student student = Student.builder()
                .name("Student")
                .age(150)
                .grade(5)
                .build();


        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(student))
                )
                .andExpect(status().is4xxClientError());

    }

    @Test
    void findByIdSuccess() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);

        mvc.perform(
                        get(URI + "/{id}", student.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(student)));
    }

    @Test
    void findByIdNotFound() throws Exception {

        mvc.perform(
                        get(URI + "/{id}", "c52e79ae-f909-4257-ac8a-e3b581b72dfe"))
                .andExpect(status().isNotFound());

    }

    @Test
    void findAll() throws Exception {
        Student student1 = createTestStudent("TestStudent1", 18, 5);
        Student student2 = createTestStudent("TestStudent2", 20, 5);

        mvc.perform(
                        get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(student1, student2)))
                );

    }

    @Test
    void updateSuccess() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);

        mvc.perform(
                        get(URI + "/{id}", student.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(student)));

        student.setAge(25);

        mvc.perform(
                        put(URI + "/{id}", student.getId())
                                .content(jackson.writeValueAsString(student))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(
                        get(URI + "/{id}", student.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(student)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        Student student = createDefaultStudent();

        mvc.perform(
                        put(URI + "/{id}", student.getId())
                                .content(jackson.writeValueAsString(student))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);

        mvc.perform(
                        delete(URI + "/{id}", student.getId().toString()))
                .andExpect(status().isOk());

    }

    @Test
    @WithAnonymousUser
    void saveNotAuthorized() throws Exception {
        Student student = createDefaultStudent();

        mvc.perform(
                post(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jackson.writeValueAsString(student))
        ).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveForbidden() throws Exception {
        Student student = createDefaultStudent();

        mvc.perform(
                post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(student))
        ).andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveSuccessAuth() throws Exception {
        Student student = createDefaultStudent();

        mvc.perform(
                post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(student))
        ).andExpect(status().isCreated());
    }

    private Student createDefaultStudent() {
        return Student.builder()
                .name("Student")
                .age(18)
                .grade(5)
                .build();
    }

    private Student createTestStudent(String name, Integer age, Integer grade) {
        Student student = Student.builder()
                .name(name)
                .age(age)
                .grade(grade)
                .build();

        repository.save(student);

        return student;
    }
}