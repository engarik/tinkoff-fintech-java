package com.gitlab.engarik.tinkoff.fintech.controller;

import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Enrollment;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.CourseRepository;
import com.gitlab.engarik.tinkoff.fintech.repository.EnrollmentRepository;
import com.gitlab.engarik.tinkoff.fintech.repository.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
class EnrollmentControllerTest {
    
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    EnrollmentRepository enrollmentRepository;

    @Autowired
    private MockMvc mvc;

    ObjectMapper jackson = new ObjectMapper();

    private final String URI = "/tinkoff/fintech/java/api/v1/enrollment/";

    @AfterEach
    public void clear() {
        enrollmentRepository.deleteAll();
    }

    @Test
    void enrollSuccess() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 3);
        Enrollment enrollment = new Enrollment(student.getId(), course.getId());

        mvc.perform(
                post(URI)
                        .content(jackson.writeValueAsString(enrollment))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void enrollValidationFail() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 0);
        Course course = createTestCourse("TestCourse", "TestDescription", 3);
        Enrollment enrollment = new Enrollment(student.getId(), course.getId());

        mvc.perform(
                        post(URI)
                                .content(jackson.writeValueAsString(enrollment))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void expel() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 0);

        enrollmentRepository.enroll(student.getId(), course.getId());

        mvc.perform(
                        delete(URI)
                                .param("studentId", student.getId().toString())
                                .param("courseId", course.getId().toString()))
                .andExpect(status().isOk());
    }

    @Test
    void getCourseList() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course1 = createTestCourse("TestCourse1", "TestDescription", 0);
        Course course2 = createTestCourse("TestCourse2", "TestDescription", 0);

        enrollmentRepository.enroll(student.getId(), course1.getId());
        enrollmentRepository.enroll(student.getId(), course2.getId());

        mvc.perform(
                        get(URI + "students/{id}", student.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(course1, course2))));

    }

    @Test
    void getStudentList() throws Exception {
        Student student1 = createTestStudent("TestStudent1", 18, 5);
        Student student2 = createTestStudent("TestStudent2", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 0);

        enrollmentRepository.enroll(student1.getId(), course.getId());
        enrollmentRepository.enroll(student2.getId(), course.getId());

        mvc.perform(
                        get(URI + "courses/{id}", course.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(student1, student2))));
    }

    @Test
    @WithAnonymousUser
    void enrollNotAuthorized() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 3);
        Enrollment enrollment = new Enrollment(student.getId(), course.getId());

        mvc.perform(
                        post(URI)
                                .content(jackson.writeValueAsString(enrollment))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "USER")
    void enrollForbidden() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 3);
        Enrollment enrollment = new Enrollment(student.getId(), course.getId());

        mvc.perform(
                        post(URI)
                                .content(jackson.writeValueAsString(enrollment))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void enrollSuccessAuth() throws Exception {
        Student student = createTestStudent("TestStudent", 18, 5);
        Course course = createTestCourse("TestCourse", "TestDescription", 3);
        Enrollment enrollment = new Enrollment(student.getId(), course.getId());

        mvc.perform(
                        post(URI)
                                .content(jackson.writeValueAsString(enrollment))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private Student createTestStudent(String name, Integer age, Integer grade) {
        Student student = Student.builder()
                .name(name)
                .age(age)
                .grade(grade)
                .build();

        studentRepository.save(student);

        return student;
    }

    private Course createTestCourse(String title, String description, Integer requiredGrade) {
        Course course = Course.builder()
                .title(title)
                .description(description)
                .requiredGrade(requiredGrade)
                .build();

        courseRepository.save(course);

        return course;
    }
}