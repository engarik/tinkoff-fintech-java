package com.gitlab.engarik.tinkoff.fintech.service;

import com.gitlab.engarik.tinkoff.fintech.controller.dto.CourseRequest;
import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.repository.CourseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.NoSuchElementException;


@SpringBootTest
@WithMockUser(roles = "ADMIN")
class CourseServiceTest {

    @Autowired
    CourseService courseService;

    @Autowired
    CourseRepository repository;

    @Test
    void getCourseFromDatabaseSaveToCache() {
        Course course = Course.builder()
                .title("CacheTestCourse")
                .description("Description")
                .requiredGrade(5)
                .build();

        repository.save(course);

        Assertions.assertThrows(NoSuchElementException.class, () -> courseService.getCourseCache().get(course.getId()).get());

        Course foundCourse = courseService.findById(course.getId());

        Assertions.assertEquals(foundCourse, course);

    }

    @Test
    void getCourseFromCache() {
        Course course = courseService.save(createFefaultCourseRequest());

        Assertions.assertEquals(courseService.getCourseCache().get(course.getId()).get(), course);
    }

    @Test
    void updateCourseCache() {
        Course course = courseService.save(createFefaultCourseRequest());

        Assertions.assertEquals(courseService.getCourseCache().get(course.getId()).get(), course);

        Course newCourse = new Course(course.getId(), "NewTitle", "Description", 0);

        courseService.update(newCourse.getId(), getFromCourse(newCourse));

        Assertions.assertEquals(newCourse, courseService.getCourseCache().get(newCourse.getId()).get());

    }

    @Test
    void deleteCourseCache() {
        Course course = courseService.save(createFefaultCourseRequest());

        Assertions.assertEquals(courseService.getCourseCache().get(course.getId()).get(), course);

        courseService.delete(course.getId());

        Assertions.assertThrows(NoSuchElementException.class, () -> courseService.getCourseCache().get(course.getId()).get());
    }

    private CourseRequest getFromCourse(Course course) {
        CourseRequest courseRequest = new CourseRequest();

        courseRequest.setTitle(course.getTitle());
        courseRequest.setDescription(course.getDescription());
        courseRequest.setRequiredGrade(course.getRequiredGrade());

        return courseRequest;
    }

    private CourseRequest createFefaultCourseRequest() {
        CourseRequest courseRequest = new CourseRequest();

        courseRequest.setTitle("CacheTestCourse");
        courseRequest.setDescription("Description");
        courseRequest.setRequiredGrade(5);

        return courseRequest;
    }

}