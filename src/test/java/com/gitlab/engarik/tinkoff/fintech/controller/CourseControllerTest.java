package com.gitlab.engarik.tinkoff.fintech.controller;


import com.gitlab.engarik.tinkoff.fintech.model.Course;
import com.gitlab.engarik.tinkoff.fintech.model.Student;
import com.gitlab.engarik.tinkoff.fintech.repository.CourseRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
class CourseControllerTest {

    @Autowired
    CourseRepository repository;

    @Autowired
    private MockMvc mvc;

    ObjectMapper jackson = new ObjectMapper();

    private final String URI = "/tinkoff/fintech/java/api/v1/courses";

    @AfterEach
    public void clear() {
        repository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        Course course = createDefaultCourse();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(course))
                )
                .andExpect(status().isCreated());

    }

    @Test
    void saveValidationFail() throws Exception {
        Course course = createDefaultCourse().toBuilder()
                .requiredGrade(10)
                .build();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(course))
                )
                .andExpect(status().is4xxClientError());

    }

    @Test
    void findByIdSuccess() throws Exception {
        Course course = createTestCourse("TestCourse", "TestDescription", 5);

        mvc.perform(
                        get("/tinkoff/fintech/java/api/v1/courses/{id}", course.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(course)));
    }

    @Test
    void findByIdNotFound() throws Exception {

        mvc.perform(
                        get("/tinkoff/fintech/java/api/v1/courses/{id}", "c52e79ae-f909-4257-ac8a-e3b581b72dfe"))
                .andExpect(status().isNotFound());

    }

    @Test
    void findAll() throws Exception {
        Course course1 = createTestCourse("TestCourse1", "TestDescription1", 5);
        Course course2 = createTestCourse("TestCourse2", "TestDescription2", 5);

        mvc.perform(
                        get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(course1, course2)))
                );

    }

    @Test
    void updateSuccess() throws Exception {
        Course course = createTestCourse("TestCourse", "TestDescription", 5);

        mvc.perform(
                        get(URI + "/{id}", course.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(course)));

        course.setRequiredGrade(0);

        mvc.perform(
                        put(URI + "/{id}", course.getId())
                                .content(jackson.writeValueAsString(course))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(
                        get(URI + "/{id}", course.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(course)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        Course course = createDefaultCourse().toBuilder()
                .title("TestCourse1")
                .build();

        mvc.perform(
                        put(URI + "/{id}", course.getId())
                                .content(jackson.writeValueAsString(course))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        Course course = createTestCourse("TestCourse", "TestDescription", 5);

        mvc.perform(
                        delete(URI + "/{id}", course.getId().toString()))
                .andExpect(status().isOk());

    }

    @Test
    @WithAnonymousUser
    void saveNotAuthorized() throws Exception {
        Course course = createDefaultCourse();

        mvc.perform(
                post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(course))
        ).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveForbidden() throws Exception {
        Course course = createDefaultCourse();

        mvc.perform(
                post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(course))
        ).andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveSuccessAuth() throws Exception {
        Course course = createDefaultCourse();

        mvc.perform(
                post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(course))
        ).andExpect(status().isCreated());
    }

    private Course createDefaultCourse() {
        return Course.builder()
                .title("TestCourse")
                .description("TestDescription")
                .requiredGrade(5)
                .build();
    }

    private Course createTestCourse(String title, String description, Integer requiredGrade) {
        Course course = Course.builder()
                .title(title)
                .description(description)
                .requiredGrade(requiredGrade)
                .build();

        repository.save(course);

        return course;
    }
}